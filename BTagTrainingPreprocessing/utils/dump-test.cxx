/// @file dump-test.cxx
/// @brief Test reading the main calo cluster and track particle container
///
/// This test simply reads in the static payload of the track particle
/// container of a primary xAOD. And checks how fast this can actually
/// be done.

// local inclues
#include "HDF5WriterAbstraction.hh"
#include "Track.hh"
#include "Jet.hh"

// System include(s):
#include <memory>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>
#include <TLorentzVector.h>

// AnalysisBase tool include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

// EDM include(s):
#include "xAODCore/tools/PerfStats.h"
#include "xAODCore/tools/IOStats.h"
#include "xAODCore/tools/ReadStats.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
#include "JetJvtEfficiency/JetJvtEfficiency.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"
#include "xAODJet/JetContainer.h"
#include "xAODTruth/TruthEventContainer.h"

int main (int argc, char *argv[])
{
	// The name of the application:
	static const char *APP_NAME = "BTagTestDumper";

	// Check that at least one input file was provided:
	if (argc < 2) {
		Error( APP_NAME, "Usage: %s <file1> [file2] ...", APP_NAME );
		return 1;
	}

	// Set up the environment:
	RETURN_CHECK( APP_NAME, xAOD::Init() );

	// Set up the event object:
	xAOD::TEvent event(xAOD::TEvent::kClassAccess);

	// Initialize JetCalibrationTool with release 21 recommendations
	JetCalibrationTool calib_tool("JetCalibrationTool");
	calib_tool.setProperty("JetCollection", "AntiKt4EMTopo");
	calib_tool.setProperty("ConfigFile", "JES_MC16Recommendation_Aug2017.config");
	calib_tool.setProperty("CalibSequence", "JetArea_Residual_EtaJES_GSC");
	calib_tool.setProperty("IsData", false);
	RETURN_CHECK( APP_NAME, calib_tool.initialize() );

	JetCleaningTool jetcleaningtool("JetCleaningTool", JetCleaningTool::LooseBad, false);
	RETURN_CHECK( APP_NAME, jetcleaningtool.initialize() );

	InDet::InDetTrackSelectionTool indettrackselectiontool("InDetTrackSelectionTool", "Loose");
	RETURN_CHECK( APP_NAME, indettrackselectiontool.initialize() );

	JetVertexTaggerTool jvttool("JetVertexTaggerTool");
	RETURN_CHECK( APP_NAME, jvttool.initialize() );

	CP::JetJvtEfficiency jvtefficiencytool("JetJvtEfficiency");
	jvtefficiencytool.setProperty("WorkingPoint", "Medium");
	jvtefficiencytool.setProperty("SFFile","JetJvtEfficiency/Moriond2017/JvtSFFile_EM.root");
	RETURN_CHECK( APP_NAME, jvtefficiencytool.initialize() );

	// Set up output file
	std::string output_file = "output.h5";
	HDF5WriterAbstraction h5writer(output_file.c_str());

	// Start the measurement:
	auto ps = xAOD::PerfStats::instance();
	ps.start();

	// Loop over the specified files:
	for (int i = 1; i < argc; ++i) {

		// Open the file:
		std::unique_ptr<TFile> ifile(TFile::Open(argv[i], "READ"));
		if ( ! ifile.get() || ifile->IsZombie()) {
			Error( APP_NAME, "Couldn't open file: %s", argv[i] );
			return 1;
		}
		Info( APP_NAME, "Opened file: %s", argv[i] );

		// Connect the event object to it:
		RETURN_CHECK( APP_NAME, event.readFrom(ifile.get()) );

		// Loop over its events:
		const Long64_t entries = event.getEntries();
		for (Long64_t entry = 0; entry < entries; ++entry) {

			// Load the event:
			if (event.getEntry(entry) < 0) {
				Error( APP_NAME, "Couldn't load entry %lld from file: %s", entry, argv[i] );
				return 1;
			}

			// Print some status:
			if ( ! (entry % 500)) {
				Info( APP_NAME, "Processing entry %lld / %lld", entry, entries );
			}

			const xAOD::TruthEventContainer *truth_events = 0;
			RETURN_CHECK( APP_NAME, event.retrieve(truth_events, "TruthEvents") );

			const xAOD::JetContainer *jets = 0;
			RETURN_CHECK( APP_NAME, event.retrieve(jets, "AntiKt4EMTopoJets") );

			for (const xAOD::Jet *jet : *jets) {
				Jet out_jet;
				xAOD::Jet *calib_jet;
				calib_tool.calibratedCopy(*jet, calib_jet);
				jvttool.updateJvt(*calib_jet);
				if ( ! jetcleaningtool.keep(*calib_jet) || ! jvtefficiencytool.passesJvtCut(*calib_jet)) {
					continue;
				}
				if (calib_jet->pt() < 20000 || fabs(calib_jet->eta()) > 2.5) {
					continue;
				}

				bool overlap_skip = false;
				for (const xAOD::TruthEvent *truth_event : *truth_events) {
					for(unsigned truth_event_index = 0; truth_event_index < truth_event->nTruthParticles(); truth_event_index++) {
						const xAOD::TruthParticle *truth_particle = truth_event->truthParticle(truth_event_index);
						if (abs(truth_particle->pdgId()) == 11) {
							if (truth_particle->pt() < 10000) {
								continue;
							}
							TLorentzVector truth_lorentz_vector;
							truth_lorentz_vector.SetPtEtaPhiM(truth_particle->pt(), truth_particle->eta(), truth_particle->phi(), truth_particle->m());
							if (calib_jet->p4().DeltaR(truth_lorentz_vector) < 0.3) {
								overlap_skip = true;
								break;
							}
						}
					}
					if (overlap_skip) {
						break;
					}
				}
				if (overlap_skip) {
					continue;
				}

				fillFlavorTaggingVariables(*calib_jet, out_jet);
				h5writer.add_jet(out_jet);

				// make a vector to store tracks to HDF5.
				std::vector<Track> tracks;
				// Read the track particles:
				const xAOD::BTagging *btagging = calib_jet->btagging();
				//const xAOD::BTagging *btagging = jet->btagging();
				auto links = btagging->auxdata<std::vector<ElementLink<xAOD::TrackParticleContainer> > >("BTagTrackToJetAssociator");
				for (const auto &link : links) {
					if(link.isValid()) {
						const xAOD::TrackParticle *tp = *link;
						if ( ! indettrackselectiontool.accept(tp)) {
							continue;
						}
						// Read in its core variables:
						Track out_track;
						out_track.pt = tp->pt();
						out_track.eta = tp->eta();
						tracks.push_back(out_track);
					}
				}
				h5writer.add_tracks(tracks);

				delete calib_jet;
			}
		}
	}

	h5writer.flush();
	h5writer.close();

	// Stop the measurement:
	ps.stop();
	xAOD::IOStats::instance().stats().Print("Summary");

	// Return gracefully:
	return 0;
}
